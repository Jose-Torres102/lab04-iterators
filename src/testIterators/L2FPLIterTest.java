	package testIterators;

	import java.util.Iterator;

import interfaces.Position;
import interfaces.PositionalList;
import linkedLists.DLDHDTListIterableF2L;
import llPositionalList.LinkedPositionalList;
import llPositionalList.PLElementsBackwardIterator;


	public class L2FPLIterTest {

		/**
		 * @param args
		 */
		public static void main(String[] args) {
			Integer[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // auto boxing....
			LinkedPositionalList<Integer> list1 = new LinkedPositionalList<Integer>(); 
			for (int i = 0; i<=a.length-1; i++) { 
				list1.addLast(a[i]); 

			}
			showElements(list1); 

			String[] b = {"Perro", "Gato", "Casa", "Lomo", "Vaca", "Buho", 
			"Pelagato"}; 
			LinkedPositionalList<String> list2 = new LinkedPositionalList<String>(); 
			for (int i = 0; i<=b.length-1; i++) { 
				list2.addLast(b[i]); 
			}
			showElements(list2); 

		}


		private static <E> void showElements(PositionalList<E> c) { 
		PLElementsBackwardIterator<E> iter = new PLElementsBackwardIterator<E>(c);
		while (iter.hasNext()) { 
		   Position<E> e = iter.next();
		   System.out.println(e.getElement()); 
		    
			}
		}
	}
