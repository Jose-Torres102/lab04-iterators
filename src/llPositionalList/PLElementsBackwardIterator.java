package llPositionalList;

import java.util.Iterator;

import interfaces.Position;
import interfaces.PositionalList;

public class PLElementsBackwardIterator<E> implements Iterator<Position<E>> {
	
	PositionalList<E> list;
	PLIteratorL2F<E> iter = null;

	public PLElementsBackwardIterator(PositionalList<E> list) {
		this.list = list;
		iter = new PLIteratorL2F<E>(list);

	}
	

	@Override
	public boolean hasNext() {
		
		return iter.hasNext();
	}

	@Override
	public Position<E> next() {

		return iter.next();
	}

}
